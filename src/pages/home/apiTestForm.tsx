import Axios from 'axios';
import { FORM_ERROR, ValidationErrors } from 'final-form';
import React, { useCallback, useState } from 'react';
import { Field, Form } from 'react-final-form';
import beautify from 'js-beautify';
import { ApiTestFormValues, PrerenderApiResponse } from './types';

const preStyles: React.CSSProperties = {
  background: '#eee',
  border: '1px solid #ccc',
  padding: '10px',
  borderRadius: '10px',
  maxWidth: '100vw',
  whiteSpace: 'break-spaces',
};

const validate = (values: ApiTestFormValues): ValidationErrors => {
  try {
    const { urlToPrerender } = values;

    // eslint-disable-next-line no-new
    new URL(urlToPrerender);
  } catch (_) {
    return {
      urlToPrerender: 'Invalid url',
    };
  }

  return {};
};

const ApiTestForm: React.FC = () => {
  const [prerenderApiReponse, setPrerenderApiReponse] = useState<
    PrerenderApiResponse
  >();
  const onSubmit = useCallback(
    async ({ urlToPrerender }: ApiTestFormValues) => {
      setPrerenderApiReponse(undefined);

      try {
        const { data } = await Axios.post<PrerenderApiResponse>(
          `/api/prerender/${encodeURIComponent(urlToPrerender)}`,
        );

        setPrerenderApiReponse(data);

        return undefined;
      } catch (error) {
        return {
          [FORM_ERROR]: `Failed to prefetch: ${error.response.data.error}`,
        };
      }
    },
    [],
  );

  return (
    <>
      <Form<ApiTestFormValues>
        {...{ validate, onSubmit }}
        render={({
          handleSubmit,
          errors,
          submitError,
          hasValidationErrors,
          submitting,
        }) => (
          <form onSubmit={handleSubmit}>
            <h2>API tester</h2>
            <div>
              {submitError && (
                <>
                  <span style={{ color: 'red' }}>{submitError}</span>
                  <br />
                </>
              )}
              <label htmlFor="urlToPrerender">Url to prerender</label>
              <br />
              <Field
                name="urlToPrerender"
                id="urlToPrerender"
                component="input"
                disabled={submitting}
                placeholder="https://google.com"
                style={{
                  width: '50vw',
                }}
              />
              <br />
              {errors.urlToPrerender && (
                <span style={{ color: 'red' }}>{errors.urlToPrerender}</span>
              )}
            </div>
            <br />
            <button disabled={hasValidationErrors || submitting} type="submit">
              Submit
            </button>
          </form>
        )}
      />
      {prerenderApiReponse && (
        <div>
          <span>Response status: {prerenderApiReponse.status}</span>
          <br />
          <span>Headers:</span>
          <pre style={preStyles}>
            {JSON.stringify(prerenderApiReponse.headers, undefined, 2)}
          </pre>
          <span>HTML Content:</span>
          <pre style={preStyles}>
            {beautify.html(prerenderApiReponse.content)}
          </pre>
        </div>
      )}
    </>
  );
};

export default ApiTestForm;
