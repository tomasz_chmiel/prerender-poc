import React from 'react';
import ApiTestForm from './apiTestForm';

const Home = (): React.ReactNode => (
  <div>
    This App allows you to prerender your client only app to HTML string.
    <br />
    <br />
    <b>POST</b> https://ssr-prerender.herokuapp.com/api/prerender/[encodedUrl]
    <br />
    <br />
    Optional body:
    <pre>
      {JSON.stringify(
        {
          cookies: {
            '<cookie-key>': '<cookie-value>',
          },
          localStorage: {
            '<localStorage-key>': '<localStorage-value>',
          },
        },
        undefined,
        2,
      )}
    </pre>
    Both fields, cookies and localStorage are optional.
    <br />
    <br />
    Example: <br />
    <b>POST</b>{' '}
    https://ssr-prerender.herokuapp.com/api/prerender/http%3A%2F%2Flocalhost%3A3000
    <br />
    <br />
    Response:
    <pre>
      {JSON.stringify(
        {
          status: 404,
          headers: {
            test: 'test-header',
          },
          content: '<html lang="en"><your-page-content /></html>',
        },
        undefined,
        2,
      )}
    </pre>
    <br />
    <br />
    By default this endpoint will wait 10 seconds for your app to fetch/render
    everything but you can speed this up. When your app is ready to be
    prerendered just set a value for <pre>window.prerenderData</pre>
    You can pass additional response headers here and override response status.
    <br />
    <br />
    Example:
    <br />
    <pre>
      {`window.prerenderData = {
  status: 404,
  headers: {
    test: 'test-header'
  }
}`}
    </pre>
    <ApiTestForm />
  </div>
);

export default Home;
