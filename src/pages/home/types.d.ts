export interface ApiTestFormValues {
  urlToPrerender: string;
}

export interface PrerenderApiResponse {
  status: number;
  headers: Record<string, string[]>;
  content: string;
}
