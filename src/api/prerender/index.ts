import { NextApiRequest, NextApiResponse } from 'next';
import { PostPrerenderEndpointShape } from './types';
import { getValidUrl, prerenderUrl, validateRequestBody } from './utils';

const handleRequest = async (
  req: NextApiRequest,
  res: NextApiResponse,
): Promise<void> => {
  if (req.method === 'POST') {
    const {
      query: { url },
      body,
    } = req;
    const parsedUrl = typeof url === 'string' ? getValidUrl(url) : null;
    const requestBody = body || {};

    if (!parsedUrl) {
      return res.status(400).json({
        error: 'Invalid url',
      });
    }

    const validationResult = validateRequestBody(requestBody);

    if (!validationResult.valid) {
      return res.status(400).json({
        errors: validationResult.errors,
      });
    }

    const { cookies, localStorage } = requestBody as PostPrerenderEndpointShape;

    try {
      return res
        .status(200)
        .json(await prerenderUrl(parsedUrl, cookies, localStorage));
    } catch (error) {
      return res.status(400).json({
        error: `Failed to prerender the page. Error: ${error.message}`,
      });
    }
  }

  return res.status(404).json({
    error: 'Not found',
  });
};

export default handleRequest;
