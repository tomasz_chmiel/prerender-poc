import { Schema } from 'jsonschema';

export const PRERENDER_POST_BODY_SCHEMA: Schema = {
  type: 'object',
  properties: {
    cookies: {
      type: 'object',
      additionalProperties: {
        type: 'string',
      },
    },
    localStorage: {
      type: 'object',
      additionalProperties: {
        type: 'string',
      },
    },
  },
};
