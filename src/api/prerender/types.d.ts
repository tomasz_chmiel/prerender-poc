export interface PrerenderedPageData {
  content: string;
  status: number;
  headers: Record<string, string | string[]>;
}

export interface PrerenderDataValue {
  status?: number;
  headers?: Record<string, string | string[]>;
}

export type CookiesRequestParam = Record<string, string>;
export type LocalStorageRequestParam = Record<string, string>;

export interface PostPrerenderEndpointShape {
  cookies?: CookiesRequestParam;
  localStorage?: LocalStorageRequestParam;
}
