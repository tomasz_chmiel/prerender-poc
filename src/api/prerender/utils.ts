import { Validator, ValidatorResult } from 'jsonschema';
import puppeteer, { Page } from 'puppeteer';
import { PRERENDER_POST_BODY_SCHEMA } from './consts';
import {
  CookiesRequestParam,
  LocalStorageRequestParam,
  PrerenderDataValue,
  PrerenderedPageData,
} from './types';

export const getValidUrl = (url: string): URL | null => {
  try {
    return new URL(url);
  } catch (_) {
    return null;
  }
};

const tryGetPrerenderDataValue = async (
  page: Page,
): Promise<PrerenderDataValue | undefined> => {
  try {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const prerenderData = (await page.evaluate('window.prerenderData')) as any;

    if (!prerenderData || typeof prerenderData !== 'object') {
      return undefined;
    }

    const { headers, status } = prerenderData;

    const validStatus =
      typeof status === 'number' && status >= 100 && status < 600
        ? status
        : undefined;
    const validHeaders =
      headers && typeof headers === 'object'
        ? Object.entries(headers).reduce((result, [key, headerValue]) => {
            if (typeof headerValue === 'string') {
              return {
                ...result,
                [key]: headerValue,
              };
            }

            if (Array.isArray(headerValue)) {
              const validHeaderValues = headerValue.filter(
                (headerSubValue) => typeof headerSubValue === 'string',
              );

              return {
                ...result,
                [key]: validHeaderValues,
              };
            }

            return result;
          }, {})
        : undefined;

    return {
      ...(validStatus ? { status: validStatus } : {}),
      ...(validHeaders && Object.keys(validHeaders).length
        ? { headers: validHeaders }
        : {}),
    };
  } catch (e) {
    return undefined;
  }
};

const populateCookiesAndLocalStorage = async (
  page: Page,
  url: URL,
  cookies: CookiesRequestParam | undefined,
  localStorage: LocalStorageRequestParam | undefined,
): Promise<void> => {
  await page.goto(url.toString(), { timeout: 10000 });

  if (cookies) {
    const puppeteerCookies = Object.entries(cookies).map(([name, value]) => ({
      name,
      value,
    }));

    await page.setCookie(...puppeteerCookies);
  }

  if (localStorage) {
    const localStorageSetScript = Object.entries(localStorage).reduce<string>(
      (result, [key, value]) =>
        `${result}window.localStorage.setItem(\`${key}\`, \`${value}\`);`,
      '',
    );

    await page.evaluate(localStorageSetScript);
  }
};

export const prerenderUrl = (
  url: URL,
  cookies: CookiesRequestParam | undefined,
  localStorage: LocalStorageRequestParam | undefined,
): Promise<PrerenderedPageData> =>
  // eslint-disable-next-line no-async-promise-executor
  new Promise<PrerenderedPageData>(async (resolve, reject) => {
    try {
      const browser = await puppeteer.launch({
        args: ['--no-sandbox', '--disable-setuid-sandbox'],
      });
      const page = await browser.newPage();

      page.setExtraHTTPHeaders({
        'X-Is-Prerender-Request': '1',
      });

      if (cookies || localStorage) {
        await populateCookiesAndLocalStorage(page, url, cookies, localStorage);
      }

      const pageResponse = await page.goto(url.toString(), { timeout: 10000 });
      const defaultStatus = pageResponse.status();
      const loadStartedTimestamp = Date.now();

      const resolvePrerender = async (prerenderData?: PrerenderDataValue) => {
        const content = await page.content();
        const { headers, status } = prerenderData || {};

        await browser.close();

        resolve({
          status: status || defaultStatus || 200,
          headers: headers || {},
          content,
        });
      };

      while (Date.now() - loadStartedTimestamp < 10000) {
        // eslint-disable-next-line no-await-in-loop
        const prerenderData = await tryGetPrerenderDataValue(page);

        if (prerenderData) {
          // eslint-disable-next-line no-await-in-loop
          resolvePrerender(prerenderData);
          return;
        }
      }

      resolvePrerender();
    } catch (error) {
      reject(error);
    }
  });

export const validateRequestBody = (body: unknown): ValidatorResult => {
  const validator = new Validator();
  const validationResult = validator.validate(body, PRERENDER_POST_BODY_SCHEMA);

  return validationResult;
};
