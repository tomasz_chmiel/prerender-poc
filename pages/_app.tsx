import React from 'react';
import { AppComponent } from 'next/dist/next-server/lib/router/router';

const MyApp: AppComponent = ({ Component, pageProps }) => (
  <Component {...pageProps} />
);

export default MyApp;
